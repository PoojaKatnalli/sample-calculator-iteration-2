# SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT


"""
Implements the console input method tests.
"""


import decimal
import io
import sys

import pytest

from sample_calculator.inputs import base
from sample_calculator.inputs.inputs import console_input


def test_get_input_values_success():
    console = console_input.ConsoleInputMethod()
    sys.stdin = io.StringIO("0; 1;2")
    assert console.get_input_values() == [decimal.Decimal(0), decimal.Decimal(1), decimal.Decimal(2)]


def test_get_input_values_invalid_input():
    console = console_input.ConsoleInputMethod()
    sys.stdin = io.StringIO("invalid")
    with pytest.raises(base.InputMethodError):
        console.get_input_values()
